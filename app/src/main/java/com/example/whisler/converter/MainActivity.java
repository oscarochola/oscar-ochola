package com.example.whisler.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button exitBtn = (Button) findViewById(R.id.Exit);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);
            }
        });

        Button convertButton = (Button) findViewById(R.id.Convert);
        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText millimeter = (EditText) findViewById(R.id.mmEditText);
                EditText inches = (EditText) findViewById(R.id.inEditText);

                double result, mm;
                mm = Double.parseDouble(millimeter.getText().toString());
                result = (mm/25.4);

                inches.setText(Double.toString(result));
            }
        });
    }
}
